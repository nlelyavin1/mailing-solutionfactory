import os

from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'SolutionFactory.settings')

app = Celery('Mailing', timezone='Europe/Moscow')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


app.conf.beat_schedule = {
    'check-mailing-every-1-minute': {
        'task': 'api.tasks.check_start_mailing',
        'schedule': crontab(minute='*/1'),

    },
}

