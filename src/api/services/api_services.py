def update_instance(*, validated_data, instance):
    for (key, value) in validated_data.items():
        setattr(instance, key, value)

    phone_number = validated_data.get('phone_number')
    code_operator = None
    if phone_number:
        code_operator = phone_number[1:4]

    instance.code_mobile_operator = code_operator if code_operator else instance.code_mobile_operator
    try:
        instance.save()
    except Exception as e:
        # TODO loguru
        print(e)
