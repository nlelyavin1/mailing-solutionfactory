from datetime import datetime
from typing import Optional


from SolutionFactory.settings import TOKEN_SOLUTION_FACTORY
from api.models import MailingList, MailMessage
from api.services import ExternalApiService


class MailingService:
    """
    Сервис для более удобной работы с моделью MailingList и отправкой рассылок
    """

    def __init__(self, *, instance_mailing: Optional[MailingList], id_mailing_list: Optional[int]):
        """
        :param instance_mailing: instance MailingList
        :param id_mailing_list: id MailingList
        :return:
        """
        self.__mailing_list = instance_mailing if instance_mailing else MailingList.objects.get(id=id_mailing_list)

    def set_status_mailing_list(self, *, status: str) -> None:
        """
        Задаем определенный status MailingList-у. Описание статусов в документации

        :param status:
        :return:
        """
        self.__mailing_list.mailing_status = status

        self.__mailing_list.save(update_fields=['mailing_status'])

    @staticmethod
    def mailing_message(*, url: str, body: dict, client_id: int, mail_message: MailMessage) -> None:
        """
        Метод отправляет запрос на определенный url

        :param client_id:
        :param mail_message:
        :param url:
        :param body: тело запроса
        :return:
        """
        token = TOKEN_SOLUTION_FACTORY
        headers = {'Authorization': f'Bearer {token}', 'Content-Type': 'application/json', 'accept': 'application/json'}

        external_api_service = ExternalApiService()
        status_response_code = external_api_service.send_notification(url=f'{url}{client_id}', body=body,
                                                                      headers=headers)

        mail_message.status = status_response_code
        mail_message.finish_at = datetime.now()
        mail_message.save(update_fields=['status', 'finish_at'])
