from typing import Optional

import requests
from requests import exceptions, Response


class ExternalApiService:
    """
    Сервис для работы с удаленным API
    """

    @staticmethod
    def __call_api_post(*, url: str, json_body: dict, headers: dict, timeout: tuple) -> Optional[Response]:
        """
        Вызов стороннего API с методом post по заданным параметрам
        :param json_body:
        :param headers:
        :param timeout:
        :return:
        """
        try:
            resp = requests.post(url=url, json=json_body, timeout=timeout,
                                 headers=headers)

            return resp
        except exceptions.Timeout:
            #     TODO logging
            pass
        except exceptions.RequestException:
            pass

    def send_notification(self, url, body, headers) -> int:
        timeout = (10, 30)
        resp = self.__call_api_post(url=url, json_body=body, headers=headers, timeout=timeout)

        status_code = 0

        if resp is not None:
            status_code = resp.status_code
        return status_code
