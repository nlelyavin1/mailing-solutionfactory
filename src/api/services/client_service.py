from django.db.models import QuerySet
from loguru import logger

from SolutionFactory.settings import ROTATION_LOG
from api.models import Client


class ClientService:

    @staticmethod
    def get_clients_from_queryset(queryset_clients: QuerySet) -> list:
        clients = list()

        try:
            for client in queryset_clients:
                clients.append(client)

        except Exception as e:
            logger.add('log/exception_log.log', format='{message} {time} {extra[e]}', rotation=ROTATION_LOG,
                       compression='zip', serialize=True)

        return clients

    @staticmethod
    def get_queryset_clients_join_mailing_list(id_mailing_list: int) -> QuerySet:
        """
        Метод возвращает обьект QuerySet содержащий всех клиентов, по фильтру указанному в рассылке.

        :param id_mailing_list:
        :return:
        """
        # id создается автоматически в бд, так что sql иньекций не может быть
        sql_script = f'''
                        SELECT c.id FROM api_client c
                        JOIN api_mailinglist m_l 
                            ON c.code_mobile_operator = m_l.code_mobile_operator 
                            AND c.tag = m_l.tag 
                        WHERE m_l.ID = {id_mailing_list};
                    '''

        return Client.objects.raw(sql_script)

    @staticmethod
    def get_client_or_none(*, id_client: int = None, phone_number: str = None) -> (Client, None):
        try:
            return Client.objects.get(id=id_client) if id_client else Client.objects.get(phone_number=phone_number)
        except Client.DoesNotExist:
            return None
