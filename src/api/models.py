from django.db import models

from .static_var import phone_number_regex, code_mobile_operator_regex
from djchoices import DjangoChoices, ChoiceItem


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        abstract = True


class BaseDateTimeStartAndFinish(models.Model):
    """
    Абстрактняа модель с временем начала и окончания какого-то события
    """
    start_at = models.DateTimeField(null=True)
    finish_at = models.DateTimeField(null=True)

    class Meta:
        abstract = True


class TagAndOperator(models.Model):
    """
    Абстрактная таблица для Client и MailingList
    """

    code_mobile_operator = models.CharField(validators=[code_mobile_operator_regex], max_length=3, blank=True,
                                            null=True, verbose_name='Код мобильного оператора клиента')
    tag = models.CharField(max_length=250, verbose_name='Тег клиента', null=True, blank=True)

    class Meta:
        abstract = True


class MailingStatus(DjangoChoices):
    new = ChoiceItem()
    in_queue = ChoiceItem()
    in_process = ChoiceItem()
    success = ChoiceItem()
    wrong = ChoiceItem()


class MailingList(BaseModel, BaseDateTimeStartAndFinish, TagAndOperator):
    """
    Модель рассылки
    :attr mailing_status: статусы, new - новая рассылка, in_queue - рассылка в очереди, in_process - рассылка началась,
                                   success - рассылка закончилась, wrong - ошибка на одной из стадий
    """

    text_mail_message = models.CharField(max_length=500, verbose_name='Текст рассылки')
    mailing_status = models.CharField(choices=MailingStatus.choices, verbose_name='Статус рассылки', blank=True,
                                      null=True, default=MailingStatus.new, max_length=25)

    def __str__(self):
        return f'id: {self.id}, text_message: {self.text_mail_message}, tag: {self.tag},' \
               f' mailing_status: {self.mailing_status}'

    def save(self, *args, **kwargs):
        """
        Если рассылка уже началась и еще не закончилась, то в ней можно переопределить только поле статуса
        :param args:
        :param kwargs:
        :return:
        """
        if self.mailing_status in ['in_queue', 'in_process']:
            kwargs['update_fields'] = ['mailing_status']
        super().save(**kwargs)


class Client(BaseModel, TagAndOperator):
    """
    Модель клиента
    """

    phone_number = models.CharField(validators=[phone_number_regex], max_length=11, unique=True,
                                    verbose_name='Номер телефона клиента')
    time_zone_utc = models.IntegerField(verbose_name='Временная зона клиента')

    def __str__(self):
        return f'id: {self.id}, phone_number: {self.phone_number}, time_zone_utc: {self.time_zone_utc}, tag: {self.tag}'


class MailMessage(BaseModel, BaseDateTimeStartAndFinish):
    """
    Модель сообщения рассылки
    """

    mailing_list_fk = models.ForeignKey(MailingList, on_delete=models.SET_NULL, verbose_name='id рассылки', null=True,
                                        blank=True, related_name='mail_message_mailing')
    client_fk = models.ForeignKey(Client, on_delete=models.SET_NULL, verbose_name='id клиента', null=True, blank=True,
                                  related_name='mail_message_client')

    status = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return f'id: {self.id}, status_message: {self.status}, mailing_list_fk: {self.mailing_list_fk}, ' \
               f'client_fk: {self.client_fk}'
