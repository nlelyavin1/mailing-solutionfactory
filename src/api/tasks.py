from datetime import datetime

from .models import MailingList, MailMessage
from SolutionFactory.celery import app
from .services import ClientService, MailingService


@app.task
def do_mailing(*, id_mailing_list: int, text_mail_message: str):
    """
    Рассылка клиентам

    :param text_mail_message: Текст сообщения
    :param id_mailing_list: Читал, что celery плохо работает с обьектами Django, так что передаю id
    :return:
    """
    url = 'https://probe.fbrq.cloud/v1/send/'
    mailing_service = MailingService(id_mailing_list=id_mailing_list, instance_mailing=None)
    mailing_service.set_status_mailing_list(status='in_process')

    queryset_clients = ClientService.get_queryset_clients_join_mailing_list(id_mailing_list=id_mailing_list)
    clients = ClientService.get_clients_from_queryset(queryset_clients=queryset_clients)

    mail_message_kwargs = {
        'start_at': datetime.now(),
        'status': 0,
        'mailing_list_fk_id': id_mailing_list,
    }

    body = {
        'id': 0,
        'phone': 0,
        'text': text_mail_message
    }

    for client in clients:
        client_id = client.id

        body['id'] = client_id
        body['phone'] = int(client.phone_number)
        mail_message_kwargs['client_fk_id'] = client_id

        mail_message = MailMessage.objects.create(**mail_message_kwargs)
        mailing_service.mailing_message(url=url, body=body, mail_message=mail_message, client_id=client_id)

    mailing_service.set_status_mailing_list(status='success')


@app.task
def check_start_mailing():
    # TODO TZ
    time_now = datetime.now()

    mailings_list = MailingList.objects.filter(start_at__lt=time_now, finish_at__gt=time_now, mailing_status='new')
    for mailing in mailings_list:
        mailing_service = MailingService(id_mailing_list=None, instance_mailing=mailing)
        mailing_service.set_status_mailing_list(status='in_queue')

        do_mailing.delay(id_mailing_list=mailing.id, text_mail_message=mailing.text_mail_message)
