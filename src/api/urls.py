from django.urls import path, include

from .views import CreateClientView, CreateMailingListView, RUDMailingListView, \
    RUDClientView, DetailStatisticMailMessage, TotalStatisticMailMessage


"""
Namespace: {
            client:...,
            mailing-list:...
            }
"""

urlpatterns = [
    path('client/', include([
        path('create/', CreateClientView.as_view(), name='create_client'),
        path('RUD/<int:pk>', RUDClientView.as_view(), name='RUD_client'),
    ]), name='client'),
    path('mailing-list/', include([
        path('create/', CreateMailingListView.as_view(), name='create_mailing_list'),
        path('RUD/<int:pk>', RUDMailingListView.as_view(), name='RUD_mailing_list'),
        path('detail-statistic/<int:pk>', DetailStatisticMailMessage.as_view(), name='detail_statistic_mailing_list'),
        path('total-statistic/', TotalStatisticMailMessage.as_view(), name='total_statistic_mailing_list'),
    ]), name='mailing-list'),
]

