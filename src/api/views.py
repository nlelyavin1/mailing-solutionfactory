import datetime

import pytz
from django.db.models import Count, F
from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Client, MailingList, MailMessage
from .serializers import ClientSerializer, CreateClientSerializer, CreateMailingSerializer, MailingListSerializer, \
    DetailStatisticMailingSerializer, TotalStatisticSerializer
from .services import ClientService


def main(request):
    return render(request=request, template_name='main.html')


"""Client"""


class CreateClientView(generics.CreateAPIView):
    """
    Create (User)
    Создание пользователя
    """
    serializer_class = CreateClientSerializer


class RUDClientView(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrive, Update, Destroy (User)
    Получение, Обновление, Удаление пользователя
    """
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


"""Mailing List"""


class CreateMailingListView(generics.CreateAPIView):
    """
    Create (MailingList)
    Создание рассылки
    """
    serializer_class = CreateMailingSerializer


class RUDMailingListView(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve, Update, Destroy of MailingList (MailingList)
    Получение, Обновление, Удаление рассылки
    """
    queryset = MailingList.objects.all()
    serializer_class = MailingListSerializer


class DetailStatisticMailMessage(APIView):
    """
    Предоставление детальной статистики по рассылке.
    (Группировка по статусам)
    """

    def get(self, request, pk):
        obj = MailingList.objects.filter(id=pk) \
            .values('id', 'start_at', 'finish_at', 'tag', 'code_mobile_operator', 'text_mail_message',
                    status=F('mail_message_mailing__status')) \
            .annotate(number_message_with_status=Count('mail_message_mailing__status'))

        serializer = DetailStatisticMailingSerializer(obj, many=True)

        return Response(serializer.data)


class TotalStatisticMailMessage(APIView):
    """
    Total stat about Mail Message
    Общая статистика по рассылкам
    """

    def get(self, request):
        """
        ## attr message_group_by_status: Сообщения сгрупированные по статусам
        ## attr previous_mailings: Количество рассылок у которых дата окончания рассылки меньше нынешнего времени
        ## attr future_mailings: Будущие рассылки (рассылки, у которых время старта в будущем)
        ## attr total_mailings: Всего рассылок
        """
        message_group_by_status = MailMessage.objects.all().values('status') \
            .annotate(number_message_with_status=Count('status'))

        mailing_status_statistic = MailingList.objects.all().values('mailing_status') \
            .annotate(number_mailing_list_with_status=Count('mailing_status')).order_by(
            '-number_mailing_list_with_status')

        mailings_dict = {
            'mailing_status_statistic': mailing_status_statistic,
            'message_group_by_status': message_group_by_status,
        }

        serializer = TotalStatisticSerializer(mailings_dict)
        return Response(serializer.data)
