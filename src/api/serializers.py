from rest_framework import serializers

from .models import Client, MailingList
from .services import update_instance


class ClientSerializer(serializers.ModelSerializer):
    """
    Сериализатор модели клиента
    """

    class Meta:
        model = Client
        fields = ('phone_number', 'time_zone_utc', 'tag')


class CreateClientSerializer(serializers.ModelSerializer):
    """
    Сериализатор модели клиента
    """

    class Meta:
        model = Client
        exclude = ('code_mobile_operator',)

    def create(self, validated_data):
        """
        Создание Client и TagAndOperator-а (связанная модель с Client-ом)

        :param validated_data:
        :return:
        """

        # Вырезаем из номера клиента, код оператора (цифры 2-4 в номере)
        code_operator = validated_data.get('phone_number')[1:4]

        client = Client.objects.create(**validated_data, code_mobile_operator=code_operator)

        return client


class MailingListSerializer(serializers.ModelSerializer):
    """
    Сериализатор модели MailingList
    """

    class Meta:
        model = MailingList
        fields = ('start_at', 'finish_at', 'code_mobile_operator', 'tag', 'text_mail_message', 'mailing_status')

    def update(self, instance: MailingList, validated_data):
        id_instance = instance.id
        update_instance(instance=instance, validated_data=validated_data)

        return MailingList.objects.get(id=id_instance)


class CreateMailingSerializer(serializers.ModelSerializer):
    """
    Сериализатор создания модели MailingList
    """

    class Meta:
        model = MailingList
        fields = ('start_at', 'finish_at', 'tag', 'code_mobile_operator', 'text_mail_message')

    def create(self, validated_data):
        mailing_list = MailingList.objects.create(**validated_data)

        return mailing_list


class DetailStatisticMailingSerializer(serializers.ModelSerializer):
    """
    Сериализатор детальной статистики расслыки
    """
    status = serializers.IntegerField()
    number_message_with_status = serializers.IntegerField()

    class Meta:
        model = MailingList
        fields = ('id', 'number_message_with_status', 'status', 'start_at', 'finish_at', 'tag', 'code_mobile_operator',
                  'text_mail_message')


class TotalStatisticSerializer(serializers.Serializer):
    """
    Сериализатор общей статистики
    """

    mailing_status_statistic = serializers.JSONField()
    # future_mailings = serializers.IntegerField()
    # total_mailings = serializers.IntegerField()
    message_group_by_status = serializers.JSONField()
