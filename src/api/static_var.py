from django.core.validators import RegexValidator

phone_number_regex = RegexValidator(regex=r"^(7)(\d{10})$")
code_mobile_operator_regex = RegexValidator(regex=r"^(\d{3})$")
