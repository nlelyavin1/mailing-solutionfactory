# Mailing. Fabrique

## Запуск в локальной среде
### Без `Docker`
1. `python3 -m venv venv` - инициализация виртуального окружения
2. `cd venv/Scripts` - вход в папку с виртуальным окружением
3. `activate` - активация виртуального окружения
4. `pip install -r requirements.txt` - установка зависимостей
5. `python src/manage.py migrate` - запуск миграций
6. `python src/manage.py runserver` - запуск приложения
7. `python src/manage.py createsuperuser` - создание суперпользователя

### С `Docker-ом`
1. `docker-compose up --no-recreate`  создание images, создание и запуск conteiner

## Основные endpoint-ы (Windows):
1. http://192.168.99.100:8000/ || http://127.0.0.1:8000/
2. http://192.168.99.100:8000/docs || http://127.0.0.1:8000/docs

