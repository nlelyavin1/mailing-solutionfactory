FROM python:3.10-slim-buster

ENV PYTHONUNBUFFERED 1


WORKDIR /app

# install requirements
COPY requirements.txt .
RUN pip install -r requirements.txt

# copy project
COPY ./src /app/src

EXPOSE 8000

#
CMD python /app/src/manage.py migrate --noinput && python /app/src/manage.py runserver 0.0.0.0:8000